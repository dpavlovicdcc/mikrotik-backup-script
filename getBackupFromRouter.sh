#!/bin/bash

#----configuration----

#identity - used in backup file name construction
IDENTITY="MikroTik_Office_Address_MT002"
#enter MikroTik ip or hostname - this script will connect to
HOST="192.168.88.1"
#enter MikroTik ftp service port - default is 21
PORT="21"
#enter ftp (mikrotik) username (probably admin of mikrotik router)
USER="adminUsername"
#enter password of mikrotik user
PASSWORD="adminPassword"
#enter export filename
FILE1="exportfile.rsc"
#enter backup filename
FILE2="backupfile.backup"
#date as a string - used in backup file name construction
DATE=$(date +"%y-%m-%d_%H-%M-%S")

#----end of configuration----



#cd in the directory where script will fetch backup files
cd /root/routers_backups


#fetch backup files
ftp -n $HOST $PORT <<END_SCRIPT
quote USER $USER
quote PASS $PASSWORD
get $FILE1
get $FILE2
quit
END_SCRIPT

#first you have manually create archive directory
cp $FILE1 ./archive/"${DATE}_${IDENTITY}_$FILE1"
cp $FILE2 ./archive/"${DATE}_${IDENTITY}_$FILE2"


#--mail--
#go in the archive dir and get last two files
cd /root/routers_backups/archive
MAILCONTENT=$(ls -Art | tail -n 2)

#write and send mail
echo -e "Content of router backup archive is (/root/routers_backups/archive): (last two files)""\n\n""$MAILCONTENT" | mailx -r sender@senderhost.com -s "Info about router backup  ($IDENTITY)" admin@adminEmailDomain.com

exit 0



